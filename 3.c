#include <stdio.h>
#include <stdlib.h>
#define MAX 100
int main()
{int mat1[MAX][MAX];
 int mat2[MAX][MAX];
 int prod[MAX][MAX];
 int i,j,k,arows,acolums,brows,bcolums,sum=0;

    printf("Enter the number of rows and colums for matrix 1:\n");
    scanf("%d %d",&arows,&acolums);
    printf("Enter the elements for matrix 1:\n");
    for(i=0;i<arows;i++)
    {
        for(j=0;j<acolums;j++)
        {
            scanf("%d",&mat1[i][j]);
        }
    }

    printf("Enter the number of rows and colums for matrix 2:\n");
    scanf("%d %d",&brows,&bcolums);
    printf("Enter the elements for matrix 2:\n");
    for(i=0;i<brows;i++)
    {
        for(j=0;j<bcolums;j++)
        {
            scanf("%d",&mat2[i][j]);
        }
    }

    //calculation
    for(i=0;i<arows;i++)
    {
        for(j=0;j<bcolums;j++)
        {
           for(k=0;k<brows;k++)
           {
               sum=sum+mat1[i][k]*mat2[k][j];
           }
           prod [i][j]=sum;
           sum=0;
        }
    }

    //print results
    for(i=0;i<brows;i++)
    {
        for(j=0;j<bcolums;j++)
        {
            printf("%d",prod[i][j]);
        }
        printf("\n");
    }

    return 0;
}

    return 0;
}
